# install dependencies
    npm install

# Usage
    // node version v10.x
    // running program
    node index.js


# 后台运行
    npm i forever -g
    forever start index.js
    forever list

# 配置账号文件
    ./account.json
